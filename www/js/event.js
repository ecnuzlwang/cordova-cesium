var camera = viewer.camera;
var scene = viewer.scene;
var timeout;
var lastTap = 0;
var zoomInAmount = 100000;
var zoomInDuration = 0.2;

camera.changed.addEventListener(function(){
	zoomInAmount=camera.positionCartographic.height/5;
});

var mc = new Hammer.Manager(viewer.canvas);

// Tap recognizer with minimal 2 taps
mc.add( new Hammer.Tap({ event: 'doubletap', taps: 2 }) );
// Single tap recognizer
mc.add( new Hammer.Tap({ event: 'singletap' }) );


// we want to recognize this simulatenous, so a quadrupletap will be detected even while a tap has been recognized.
mc.get('doubletap').recognizeWith('singletap');
// we only want to trigger a tap, when we don't have detected a doubletap
mc.get('singletap').requireFailure('doubletap');


mc.on("doubletap", function(ev) {
	var windowCoords = new Cesium.Cartesian2(ev.center.x,ev.center.y);
	var groundcartesian = camera.pickEllipsoid(windowCoords, scene.globe.ellipsoid);
	var direction = new Cesium.Cartesian3();
	Cesium.Cartesian3.subtract(groundcartesian, camera.position, direction);
	var directionNor = new Cesium.Cartesian3();
	Cesium.Cartesian3.normalize(direction,directionNor);
	var zoomInVector = new Cesium.Cartesian3();
	Cesium.Cartesian3.multiplyByScalar(directionNor,zoomInAmount,zoomInVector);
	var dest = new Cesium.Cartesian3();
	Cesium.Cartesian3.add(camera.position,zoomInVector,dest);
	camera.flyTo({
		destination:dest,
		orientation:{
			direction:camera.direction,
			up:camera.up
		},
		duration:zoomInDuration
	});
});